(function($, w, H) {
  // Error-free console logs
  function log(message) {
    try { console.info(message); }
    catch (e) {}
    finally { return; }
  }

  // serialize form data to json
  $.fn.serializeJSON = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };

  /* Validation */
  var Validator;

  function validationInit() {

    // escape if jquery validate is not loaded
    if(!$.validator) {
      log('jQuery.validate is not loaded, skipping validator');
      return;
    }

    // jQuery Validate
    // https://jqueryvalidation.org/category/validator

    setupValidationRules();
  }

  function setupValidationRules() {
    var $form = $('#form_passwordupdate');

    Validator = $form.validate({
      rules: {
        user_newpassword_confirm: {
          equalTo: 'input[name=user_newpassword]'
        }
      },
      messages: {
        user_newpassword_confirm: {
          equalTo: 'The password you entered does not match'
        }
      },
      submitHandler: requestPasswordUpdate
    });
  }

  // TODO: update submit handler
  function requestPasswordUpdate(form) {

    // show a loading indicator
    $(form).addClass('form-loading');

    // do your ajax request here
    setTimeout(function() {
      // on success
      $(form).removeClass('form-loading').addClass('form-success');
    }, 1000);
  }


  $(function() {
    validationInit();
  });

})(jQuery, window, Handlebars);
