(function($, w, H) {
  // Error-free console logs
  function log(message) {
    try { console.info(message); }
    catch (e) {}
    finally { return; }
  }

  // serialize form data to json
  $.fn.serializeJSON = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };

  /* Validation */
  var Validator;

  function validationInit() {

    // escape if jquery validate is not loaded
    if(!$.validator) {
      log('jQuery.validate is not loaded, skipping validator');
      return;
    }

    // jQuery Validate
    // https://jqueryvalidation.org/category/validator

    // custom regex check
    $.validator.addMethod('regex', function(value, element, regexpr) {
      return (value) ? regexpr.test(value) : true;
    }, 'Pattern did not match.');

    setupValidationRules();
  }

  function setupValidationRules() {
    var $form = $('#form_signup');

    $.validator.addMethod("noMoreThan", function(value, element, target) {
      var eq = $(target).val();
      return (eq) ? +value < +eq : true;
    }, 'Value can\'t be higher than target.');

    $.validator.addMethod("noLessThan", function(value, element, target) {
      var eq = $(target).val();
      return (eq) ? +value > +eq : true;
    }, 'Value can\'t be less than target.');

    Validator = $form.validate({

      errorPlacement: function(err, el) {
        if(el.attr('type') === 'checkbox') {
          el.parents('label').after(err);
          return;
        }
        if(el.attr('type') === 'file') {
          el.parents('.form-group').append(err);
          return;
        }
        err.insertAfter(el);
      },

      rules: {
        email_confirm: {
          equalTo: '#email' // make sure id matches the front end
        },
        company_website: {
          regex: /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/
        },
        postal_code: {
          regex: /^[0-9]{6}$/
        },
        mobile_number: {
          regex: /^[0-9]{8}$/
        },
        discount_offer: {
          number: true,
          regex: /^\d{0,10}(\.\d{0,2})?$/,
          noMoreThan: '[name=discount_original]'
        },
        discount_offer_1: {
          number: true,
          regex: /^\d{0,10}(\.\d{0,2})?$/,
          noMoreThan: '[name=discount_original_1]'
        },
        discount_original: {
          number: true,
          regex: /^\d{0,10}(\.\d{0,2})?$/,
          noLessThan: '[name=discount_offer]'
        },
        discount_original_1: {
          number: true,
          regex: /^\d{0,10}(\.\d{0,2})?$/,
          noLessThan: '[name=discount_offer_1]'
        },
        deal_price: {
          number: true,
          regex: /^\d{0,10}(\.\d{0,2})?$/
        },
        deal_price_1: {
          number: true,
          regex: /^\d{0,10}(\.\d{0,2})?$/
        },
        purchase_online_field: {
          regex: /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/
        },
        purchase_online_field_1: {
          regex: /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/
        },
        password: {
          regex: /^[a-z0-9]{8,}$/
        },
        password_confirm: {
          equalTo: '#password' // make sure id matches the front end
        }
      },

      messages: {
        company_website: {
          regex: 'You have entered an invalid URL.',
        },
        email: {
          email: 'You have entered an invalid email.'
        },
        email_confirm: {
          equalTo: 'The email address you have entered does not match.'
        },
        postal_code: {
          regex: 'You have entered an invalid postal code.'
        },
        mobile_number: {
          regex: 'You have entered an invalid mobile no.'
        },
        agree: 'Please accept the Terms and Conditions.',
        discount_offer: {
          number: 'Please enter a valid number.',
          regex: 'Only accepts up to 2 decimal places.',
          noMoreThan: 'Offer price cannot be equal or higher than the original price'
        },
        discount_offer_1: {
          number: 'Please enter a valid number.',
          regex: 'Only accepts up to 2 decimal places.',
          noMoreThan: 'Offer price cannot be equal or higher than the original price'
        },
        discount_original: {
          number: 'Please enter a valid number.',
          regex: 'Only accepts up to 2 decimal places.',
          noLessThan: 'Original price cannot be equal or lower than the offer price'
        },
        discount_original_1: {
          number: 'Please enter a valid number.',
          regex: 'Only accepts up to 2 decimal places.',
          noLessThan: 'Original price cannot be equal or lower than the offer price'
        },
        deal_price: {
          number: 'Please enter a valid number.',
          regex: 'Only accepts up to 2 decimal places.'
        },
        deal_price_1: {
          number: 'Please enter a valid number.',
          regex: 'Only accepts up to 2 decimal places.'
        },
        purchase_online_field: {
          regex: 'You have entered an invalid URL.',
        },
        purchase_online_field_1: {
          regex: 'You have entered an invalid URL.',
        },
        password: {
          regex: 'Please enter a minimum 8 characters with alphabets and numbers.'
        },
        password_confirm: {
          equalTo: 'The password you have entered does not match.'
        },
      }
    });

    // $('#form_continue').on('click', function() {
    //   // validate
    //   if ($form.valid()) {
    //     $form.removeClass('form-step-1').addClass('form-step-2');
    //     $('html,body').animate({ scrollTop: 0 }, 400);
    //   }

    // });

    // Add Offer Toggle
    $('#form_add').on('click', function() {
      var $this = $(this);
      var $fields = $('.form-offer-2').find('textarea, input, select').not('[name^=purchase_storefront]');
      var $formAddIcon = $this.find('i');

      if($this.is('.btn-remove')) {
        $this.removeClass('btn-remove').addClass('btn-add')
          .find('span').text('Add your second offer');
        $formAddIcon.removeClass('glyphicon-minus').addClass('glyphicon-plus');
      } else {
        $this.removeClass('btn-add').addClass('btn-remove')
          .find('span').text('Remove your second offer');
        $formAddIcon.removeClass('glyphicon-plus').addClass('glyphicon-minus');
      }

      $fields.prop('disabled', false);
      $form.find('.form-offer-2').slideToggle();
    })
  }

  function setupFormPreview() {
    if(!H) return;
    H.registerHelper('breaklines', function(text) {
      text = Handlebars.Utils.escapeExpression(text);
      text = text.replace(/(\r\n|\n|\r)/gm, '<br>');
      return new Handlebars.SafeString(text);
    });

    Handlebars.registerHelper({
      eq: function (v1, v2) { return v1 == v2; },
      or: function (v1, v2) { return v1 || v2; },
      and: function (v1, v2) { return v1 && v2; }
    });

    $(document).on('show.bs.modal', function(event) {
      var data = $.extend({}, $('#form_signup').serializeJSON(), window.FormData_1);

      log (data);

      var source = $('#productTemplate').html();
      var previewTemplate = H.compile(source);

      $('.product-preview').html(previewTemplate(data));
    })
  }

  function setupTextareaCounter() {
    $('textarea').each(function() {
      var $input = $(this);
      var $max = $input.attr("maxlength");
      var $counter = $input.siblings('.textarea-counter');

      if (!$counter.length) return;

      $input.on('keyup', function() {
        $counter.trigger('update', [ $input.val().length ]);
      })

      $counter.on('update', function(event, value) {
        $(this).find('span').text( $max - value );
      });

      $counter.trigger('update', [ $input.val().length ]);

    });
  }

  function fileUploadInit() {
    if (!$.fn.fileupload) {
      log('jQuery.fileupload is not loaded, skipping...');
      return;
    }

    // which classes to trigger a file explorer prompt
    var selectFileClasses = [
      '.file-actions--default .file-photo',
      '.file-actions--upload .file-photo',
      '.file-actions--default .file-button'
    ].join();

    $('.file-upload').each(function() {
      var $this = $(this);
      var $trigger = $this.find('input');

      $this.on('click', selectFileClasses, function(e) {
        $(e.delegateTarget).find('input').click();
      })

      var uploadOptions = {
        dataType: 'text',
        autoUpload: false,
        replaceFileInput: false,
        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
        // TODO: limit file size and image dimensions
        maxFileSize: 5000000,        // 5MB Limit
        minWidthHeight: [640, 640]    // 640px x 640px
      }

      $trigger.fileupload(uploadOptions)

        .on('fileuploadadd', function(e, data) {
          $.each(data.files, function(index, file) {
            var reader = new FileReader();

            // Image dimension validation
            reader.readAsDataURL(data.files[0]);
            reader.data = data;
            reader.file = data.files[0];

            reader.onload = function (_image) {
              var image = new Image();

              image.src = _image.target.result;
              image.file = this.file;
              image.data = this.data;
              image.onload = function () {
                var w = this.width,
                  h = this.height,
                  n = this.file.name;

                if ( w < uploadOptions.minWidthHeight[0] || h < uploadOptions.minWidthHeight[1] ) {
                  var errorMessage = 'The image should at least be '
                    + uploadOptions.minWidthHeight[0] + 'x' + uploadOptions.minWidthHeight[1] + ' pixels!';
                  // TODO: error handler
                  alert(errorMessage);
                  reader.data.files.pop();

                  // revert UI
                  $('.file-actions', $this)
                    .removeClass(function (index, className) {
                      return (className.match (/(^|\s)file-actions--\S+/g) || []).join(' ');
                    })
              .addClass('file-actions--default');
                } else {

                  $('.file-photo-name', $this).text(file.name);
                  $('.file-actions', $this)
                    .removeClass(function (index, className) {
                      return (className.match (/(^|\s)file-actions--\S+/g) || []).join(' ');
                    })
                    .addClass('file-actions--upload');

                  $('.file-button-upload', $this)
                    .off('click') // unbind click to allow only one upload
                    .on('click', function () { data.submit(); });

                }
              };
            }

          });
        })

        .on('fileuploadprocessfail', function(e, data) {
            $.each(data.files, function(index, file) {
              alert(file.error);
              data.files.splice(index, 1);
            });
            // reset form
            $('.file-actions', $this)
              .removeClass(function (index, className) {
                return (className.match (/(^|\s)file-actions--\S+/g) || []).join(' ');
              })
              .addClass('file-actions--default');
          })

          .on('fileuploadstart', function (e, data) {
            $('.file-actions', $this)
              .removeClass(function (index, className) {
                return (className.match (/(^|\s)file-actions--\S+/g) || []).join(' ');
              })
              .addClass('file-actions--start');
            log('started');
          })

          .on('fileuploaddone', function (e, data) {
            // TODO: Place your server side response here

            // The upload server should respond with the image data particularly
            // - the status of the upload
            // - the URL of the image that was successfully uploaded

            // TODO: put failure logic here so that it halts
            // ex if(!data.results && !data.results.stats != "ok" && !data.resuts.image_url) { ... }
            if (!data.result) {
              return;
            }

            // This updates the UI for upload success
            $trigger.prop('required', false);
            $('.file-actions', $this)
              .removeClass(function (index, className) {
                return (className.match (/(^|\s)file-actions--\S+/g) || []).join(' ');
              })
              .addClass('file-actions--complete');
          })

          .on('fileuploadfail', function() {
            // do something on error
            alert('error');

            // reset to default
            $('.file-actions', $this)
              .removeClass(function (index, className) {
                return (className.match (/(^|\s)file-actions--\S+/g) || []).join(' ');
              })
              .addClass('file-actions--default');
          });
    });
  }

  function setupDealsDiscountToggle() {
    $('.form-offer-pricing').each(function() {
      var $discounts = $('[name^=discount_]', $(this)).not('[name^=discount_percent]');
      var $deals = $('[name^=deal_]', $(this));

      $discounts.each(function(){
        $(this).on('change', function() {
          if ($(this).val() != '') {
            $deals.each(function() {
              $(this).prop('disabled', true)
                .prop('required', false);
              $(this).valid();
            });
          } else {
            $deals.each(function() {
              $(this).prop('disabled', false)
                .prop('required', true);
            });
          }
        });
      });

      $deals.each(function(){
        $(this).on('change', function() {
          if ($(this).val() != '') {
            $discounts.each(function() { $(this).prop('disabled', true); })
          } else {
            $discounts.each(function() { $(this).prop('disabled', false); })
          }
        });
      });
    })
  }

  function setupDiscountLogic() {
    $('.form-offer-pricing').each(function() {
      var $baseField = $('[id^=discount_original]', $(this));
      var $offerField = $('[id^=discount_offer]', $(this));
      var $discountField = $('[id^=discount_percent]', $(this));

      $baseField.add($offerField).on('change', function() {
        var base = $baseField.val();
        var offer = $offerField.val();

        if (!offer.length || !base.length) return;

        var percent = ($baseField.val() - $offerField.val()) / $baseField.val() * 100;
        var round = Math.round(percent);
        $discountField.val(round);
      });

    });
  }

  function setupRadioWithText() {
    $('.radio').on('change', 'input[type=radio]', function(e, i) {
      var name = $(e.target).attr('name');
      var $text = $(e.target).parents('.radio').find('input[type=text]');
      var $rival = $('[name=' + name + ']').not(':checked');
      var $rivalText = $rival.parents('.radio').find('input[type=text]');

      $text.prop('disabled', false).prop('required', true).focus();
      $rivalText.prop('disabled', true);
      Validator.resetForm();
    });
  }

  function setupPasswordFieldToggle() {
    var $password = $('input[type=password]');

    $password.each(function() {
      var $this = $(this)
      var $toggle = $('<a/>', {
        'class': 'password-toggle',
        html: $('<span/>', { 'class': 'eye' }),
        click: function(e) {
          $this.attr('type', $this.attr('type') === 'password' ? 'text' : 'password');
        }
      })

      $(this).before($toggle)
    })

  }

  $(function() {
    fileUploadInit();
    setupTextareaCounter();
    setupDealsDiscountToggle();
    setupDiscountLogic();
    setupFormPreview();
    setupRadioWithText();
    setupPasswordFieldToggle();
    validationInit();
  });
})(jQuery, window, Handlebars);
