(function($, w, console, HB) {
  // Error-free console logs
  function log(message) {
    try { console.info(message); }
    catch (e) {}
    finally { return; }
  }

  function tooltipsInit() {
    $("[rel=tooltip]").tooltip();
  }

  function menuToggleInit() {
    $('.menu-toggle').on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass('open');
    })
  }

  function searchCompletionInit() {
    // sample data only
    var sample = [ 'lemon squeezer', 'lemon essential oil for', 'lemon balm', 'lemon juice', 'lemon juice squeezer', 'lemon scent' ];

    var dataset = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: sample,
      // todo: provide remote dataset for suggestions here
      // remote: {
      //   url: '../data/films/queries/%QUERY.json',
      //   wildcard: '%QUERY'
      // }
    });

    $('#search').typeahead({
      hint: true,
      highlight: true,
      minLength: 3
    },
    {
      name: 'search',
      source: dataset,
      templates: {
        header: '<span>Popular searches</span>'
      }
    });
  }

  function productSorterInit() {
    var $sorter = $('.sidebar-sorter');

    $sorter.on('change', '[name=sortby]', function(e) {
      e.preventDefault();

      // sort mechanism here
      console.log('sort by ' + $(this).val());
    })
  }

  function carouselInit() {
    $('.carousel').carousel({ interval: 6000});
  }

  // document ready
  $(function() {
    tooltipsInit();
    searchCompletionInit();
    menuToggleInit();
    carouselInit();
    $('.nav-header').on('click', function() {
      $(this).toggleClass('nav-active');
    });
    productSorterInit();
  });

})(jQuery, window, window.console, window.Handlebars);
